# StartX （已更新至v1.2.1）

- v1.0.0 支持http(s)
- v1.1.0 支持ws(s)
- v1.1.1 同时支持http(s),ws(s)
- v1.2.1 支持文本socket，具体参照代码

#### 项目介绍
基于Netty和Spring开发的服务端容器，支持Socket，WebSocket(SSL)，HTTP(S)，提供集成环境，能很快速的进入业务开发，对业务开发者透明，支持对各种通讯协议进行定制

#### 软件架构

代码由以下模块组成
- point          请求处理
- config         系统配置
- netty          网络通信
- system         系统配置类
- tools          系统工具
- Bootstrap      启动入口

#### 安装教程

- 导入maven项目
- 打开Bootrap类
- 新建一个测试类
- 创建main函数
- 调用Bootstrap.start()启动程序


控制台日志：
```
2018-05-09 13:06:05 INFO  com.startx.core.netty.server.impl.HttpServer  - StartxConfig [port=8081, boss=2, worker=4, springPath=classpath*:application.xml, endPoint=/, isSSL=false, resource=/resource, jksPwd=your.pwd, jksPath=/your.jks, packages=com.startx, serverClass=com.startx.core.netty.server.impl.HttpServer]
2018-05-09 13:06:05 INFO  org.springframework.context.support.ClassPathXmlApplicationContext  - Refreshing org.springframework.context.support.ClassPathXmlApplicationContext@4bec1f0c: startup date [Wed May 09 13:06:05 CST 2018]; root of context hierarchy
2018-05-09 13:06:05 INFO  org.springframework.beans.factory.xml.XmlBeanDefinitionReader  - Loading XML bean definitions from URL [file:/E:/zhangminghu/startx/target/classes/application.xml]
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def2_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def3_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def4_JSON
2018-05-09 13:06:05 INFO  com.startx.core.accesspoint.factory.AccessPointFactory  - GET_/abc/def1_JSON
2018-05-09 13:06:06 INFO  com.startx.core.netty.server.impl.HttpServer  - 
**************************    *********                     ******  ********************************
**************************  ***********                     ******  ********************************
**************************  ********* **************************************************************
**************************  ***   **************************************** *************************
**************************  ***     ********             *********     *** *************************
**************************   ***                                      **** *************************
**************************   ***                                      ***  *************************
**************************  ***                                       ***  *************************
**************************  ***                                        *** *************************
************************** ****                                        *** *************************
************************** ***                                         *** *************************
************************** ***     *****                    ****       *** *************************
************************** ***    ********               *********      ****************************
*****************************      *******  *****  ***** *********      ****************************
*****************************        **** ***********************       ****************************
*****************************            *******************            ****************************
****************************             *******************             ***************************
****************************             *******************             ***************************
****************************             *******************             ***************************
****************************              *****************              ***************************
****************************                ************                 ***************************
**************************  ***                                        *** *************************
**************************   ***                V1.1.1                ***  *************************
**************************   ****                                     **   *************************
****************************  ***   ******************************   ***   *************************
**********************************  ************************************   *************************
**************************************  *******************************    *************************
******************************* ******                        *******      *************************
2018-05-09 13:06:06 INFO  com.startx.core.netty.server.impl.HttpServer  - 服务已启动，端口号： 8081.
```

#### 更详细的说明

- 修改配置文件

```
#项目启动端口
port=8081
#spring配置路径
springPath=classpath*:application.xml
#项目访问前缀（已实现）
endPoint=/j4
#是否开启ssl
isSSL=false
#若开启ssl，需要配置如下两个参数
jksPwd=your.pwd
jksPath=/your.jks
#配置使用websocketServer，同时支持ws(s)和http(s)
serverClass=com.startx.core.netty.server.impl.WebSocketServer
```

- 更多配置项

```
package com.startx.core.system.model;

public class StartxConfig {
	/**
	 * 监听端口
	 */
	private int port = 8080;
	/**
	 * boss线程数
	 */
	private int boss = 2;
	/**
	 * workder线程数
	 */
	private int worker = 4;
	/**
	 * spring配置路径
	 */
	private String springPath = "classpath*:application.xml";
	/**
	 * 项目访问前缀
	 */
	private String endPoint = "/";
	/**
	 * 是否为ssl
	 */
	private boolean isSSL = false;
	/**
	 * 静态资源目录
	 */
	private String resource = "/resource";
	// 以下配置在ssl连接时使用
	/**
	 * jsk密码
	 */
	private String jksPwd;
	/**
	 * jsk路径
	 */
	private String jksPath;
	/**
	 * endpoint扫描路径
	 */
	private String packages = "com.startx";
	/**
	 * 设置NettyServer启动类
	 */
	private String serverClass = "com.startx.core.netty.server.impl.HttpServer";
}

```
- log4j配置默认即可,spring只需要开启Annotation

#### 代码测试
- 打开演示代码J4.java,右键即可运行

```
package com.startx.test;

import com.startx.core.Bootstrap;

public class J4 {
	
	public static void main(String[] args) throws Exception {
		
		Bootstrap.start();
		
	}
	
}
```

- Action集成测试

```
package com.startx.action;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;

import com.startx.core.accesspoint.anotation.AccessPoint;
import com.startx.core.accesspoint.anotation.RequestMethod;
import com.startx.core.accesspoint.anotation.RequestPoint;
import com.startx.core.accesspoint.anotation.ResponseType;
import com.startx.core.netty.output.http.JsonOutput;
import com.startx.core.system.Colorfulogo;
import com.startx.core.system.param.HttpArgs;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseStatus;

@Controller
@AccessPoint("/login")
public class LoginAction {
	
	@RequestPoint(value={"/empty/param"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void emptyParam() {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/body/ctx/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test02(byte[] body,ChannelHandlerContext ctx,HttpArgs args) throws UnsupportedEncodingException {
		
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		JsonOutput.object(ctx, HttpResponseStatus.OK,response);
		
	}
	
	@RequestPoint(value={"/args/body"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test03(HttpArgs args,byte[] body) {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public Map<String,Object> test04(HttpArgs args) {
		
		System.out.println("login... ");
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		
		return response;
	}
	
}

```

可以通过Rest工具进行调用，提供基本的参数

- body 请求体数据
- ctx  连接通道
- args 请求uri参数和header参数
- args 代码

```
package com.startx.core.system.param;

import java.util.Map;

/**
 * http请求参数
 */
public class HttpArgs {
	/**
	 * Http请求头
	 */
	private Map<String, String> headers;
	/**
	 * URI 参数
	 */
	private Map<String, String> params;
	/**
	 * 远端IP
	 */
	private String ip;
	/**
	 * 请求地址
	 */
	private String domain;

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}


```

#### 后续

- 提供打包启动方法 

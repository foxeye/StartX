package com.startx.core.system.param;

import java.util.Map;

/**
 * http请求参数
 */
public class HttpArgs {
	/**
	 * Http请求头
	 */
	private Map<String, String> headers;
	/**
	 * URI 参数
	 */
	private Map<String, String> params;
	/**
	 * 远端IP
	 */
	private String ip;
	/**
	 * 请求地址
	 */
	private String domain;

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
